//#include "Timer.h"
#include "SoftwareSerial.h"
#include "Arduino.h"
//#define debug
//typedef void (lifi::sendData)(void);
class lifi
{
	private:
		const byte tx,rx,in;//Transmit Receive and Photodiode input pins
		const char myport;//Port no to be given for this object
		byte ci,pi;//variables to store current and previous values of the photodiode for differential algorithm
		const char get[9];//String to send for data request to pi
		const char postStart[5];//String starting pattern to search in reply
		const char postEnd[5];//String ending pattern to search in reply
        static const unsigned int baud=2400;
        static const unsigned long int wait=3000;//Wait this much time before initiating data transfer
		SoftwareSerial *serial;//Pointer to the software serial port
        bool blEnabled=false;
        unsigned long LastTime;
        char buffer[256];//Buffer to store the incoming data
        char ch;//char buffer for incoming data
        static const char delimiter = 4;//EOT character for incoming serial data
	public:
		lifi(byte rxpin, byte txpin, byte inpin, char portno) : tx(txpin),rx(rxpin),in(inpin),myport(portno),get {'<','G',portno,'>','<','/','G','>',0}, postStart{'<','P',portno,'>',0},postEnd{'<','/','P','>',0}
		{
            #ifdef debug
            Serial.println("Initialized!");
            Serial.print("RX,TX,IN,port: ");
            char temp [15];
            sprintf(temp,"%u,%u,%u,%c",rx,tx,in,myport);
            Serial.println(temp);
            Serial.print("GET: ");
            Serial.println(get);
            Serial.print("Posts: ");
            Serial.println(postStart);
            Serial.print("Poste: ");
            Serial.println(postEnd);
            #endif
            //Initialize photodiode input pin to pull up input
            pinMode(in,INPUT);
            digitalWrite(in,1);//Turn on pull up
            ci=1;//current input of photodiode 1=>light on , 0=>light off
            pi=1;//previous input of photodiode
            serial = new SoftwareSerial(rx,tx);//Initialize Soft Serial Port
            serial->begin(baud);//Set baud rate
		}
   void readPin()
   {
        ci = digitalRead(in);//Read photodiode value
        switch(ci-pi)
        {
            case -1://-ve edge i.e. sensor blocked by receiver
                timerStart();//Start timer
                break;
            case 0://no change either off or on continuously
                if(blEnabled) timerUpdate();//Update timer if on
                break;
            case 1://+ve edge receiver removed from above the sensor
                if(blEnabled) timerStop();//Stop timer if on
                break;
        }
        pi=ci;//Save last value of photodiode
   }
   void sendData()
   {
        byte length = getData();
        if (length >0)
        {
            buffer[length]=0;//Terminate char arry for string compatability
            #ifdef debug
            Serial.println(buffer);
            #endif
            serial->println(buffer);//Send data to led port
        }
   }
   byte getData()
   {
        Serial.print(get);//Send request to pi for the data
        Serial.write(4);
        delay(1000);//Wait 1 second
        return Serial.readBytesUntil(delimiter,buffer,254);//Read upto 254 bytes or terminate on 04
   }
   void timerStart()
   {
        LastTime = millis();
        blEnabled=true;
   }
   void timerStop()
   {
        blEnabled=false;
   }
   void timerUpdate()
   {
        if(timerTick())
        sendData();
   }
   bool timerTick()
   {
        if(!blEnabled) return false;
        if ((unsigned long int)(millis() - LastTime) >= wait)
        {
            blEnabled=false;
            return true;
        }
        return false;
   }
};
