# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'newlifi.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import sys,serialenum,serial,dialog,os,threading
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_LiFiReceiver(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)
        self.portlist=[]
        self.ser= serial.Serial()
        self.timer = QtCore.QTimer()
        self.portOpen = False
        self.saveFile = dialog.Ui_updateFileDialog()
        self.edit1=False
        self.edit2=False
        self.edit3=False
        self.edit4=False
        self.name1=''
        self.name2=''
        self.name3=''
        self.name4=''
        self.buffer1=''
        self.buffer2=''
        self.buffer3=''
        self.buffer4=''
    def setupUi(self, LiFiReceiver):
        LiFiReceiver.setObjectName(_fromUtf8("LiFiReceiver"))
        LiFiReceiver.resize(480, 320)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(LiFiReceiver.sizePolicy().hasHeightForWidth())
        LiFiReceiver.setSizePolicy(sizePolicy)
        LiFiReceiver.setMinimumSize(QtCore.QSize(480, 320))
        self.verticalLayout = QtGui.QVBoxLayout(LiFiReceiver)
        self.verticalLayout.setSizeConstraint(QtGui.QLayout.SetNoConstraint)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.portLabel = QtGui.QLabel(LiFiReceiver)
        self.portLabel.setObjectName(_fromUtf8("portLabel"))
        self.horizontalLayout_2.addWidget(self.portLabel)
        self.portCB = QtGui.QComboBox(LiFiReceiver)
        self.portCB.setEnabled(True)
        self.portCB.setObjectName(_fromUtf8("portCB"))
        self.horizontalLayout_2.addWidget(self.portCB)
        self.baudLabel = QtGui.QLabel(LiFiReceiver)
        self.baudLabel.setObjectName(_fromUtf8("baudLabel"))
        self.horizontalLayout_2.addWidget(self.baudLabel)
        self.baudCB = QtGui.QComboBox(LiFiReceiver)
        self.baudCB.setObjectName(_fromUtf8("baudCB"))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.baudCB.addItem(_fromUtf8(""))
        self.horizontalLayout_2.addWidget(self.baudCB)
        self.portButton = QtGui.QPushButton(LiFiReceiver)
        self.portButton.setObjectName(_fromUtf8("portButton"))
        self.horizontalLayout_2.addWidget(self.portButton)
        self.aboutButton = QtGui.QPushButton(LiFiReceiver)
        self.aboutButton.setDefault(False)
        self.aboutButton.setFlat(False)
        self.aboutButton.setObjectName(_fromUtf8("aboutButton"))
        self.horizontalLayout_2.addWidget(self.aboutButton)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.labelbay1 = QtGui.QLabel(LiFiReceiver)
        self.labelbay1.setAlignment(QtCore.Qt.AlignCenter)
        self.labelbay1.setObjectName(_fromUtf8("labelbay1"))
        self.horizontalLayout.addWidget(self.labelbay1)
        self.file1Button = QtGui.QPushButton(LiFiReceiver)
        self.file1Button.setObjectName(_fromUtf8("file1Button"))
        self.horizontalLayout.addWidget(self.file1Button)
        self.bay1button = QtGui.QPushButton(LiFiReceiver)
        self.bay1button.setObjectName(_fromUtf8("bay1button"))
        self.horizontalLayout.addWidget(self.bay1button)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.plainTextEditbay1 = QtGui.QPlainTextEdit(LiFiReceiver)
        self.plainTextEditbay1.setTextInteractionFlags(QtCore.Qt.TextSelectableByKeyboard|QtCore.Qt.TextSelectableByMouse)
        self.plainTextEditbay1.setObjectName(_fromUtf8("plainTextEditbay1"))
        self.verticalLayout_3.addWidget(self.plainTextEditbay1)
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.labelbay3 = QtGui.QLabel(LiFiReceiver)
        self.labelbay3.setAlignment(QtCore.Qt.AlignCenter)
        self.labelbay3.setObjectName(_fromUtf8("labelbay3"))
        self.horizontalLayout_5.addWidget(self.labelbay3)
        self.file3Button = QtGui.QPushButton(LiFiReceiver)
        self.file3Button.setObjectName(_fromUtf8("file3Button"))
        self.horizontalLayout_5.addWidget(self.file3Button)
        self.bay3button = QtGui.QPushButton(LiFiReceiver)
        self.bay3button.setObjectName(_fromUtf8("bay3button"))
        self.horizontalLayout_5.addWidget(self.bay3button)
        self.verticalLayout_5.addLayout(self.horizontalLayout_5)
        self.plainTextEditbay3 = QtGui.QPlainTextEdit(LiFiReceiver)
        self.plainTextEditbay3.setTextInteractionFlags(QtCore.Qt.TextSelectableByKeyboard|QtCore.Qt.TextSelectableByMouse)
        self.plainTextEditbay3.setObjectName(_fromUtf8("plainTextEditbay3"))
        self.verticalLayout_5.addWidget(self.plainTextEditbay3)
        self.verticalLayout_3.addLayout(self.verticalLayout_5)
        self.gridLayout.addLayout(self.verticalLayout_3, 0, 0, 1, 1)
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.labelbay2 = QtGui.QLabel(LiFiReceiver)
        self.labelbay2.setAlignment(QtCore.Qt.AlignCenter)
        self.labelbay2.setObjectName(_fromUtf8("labelbay2"))
        self.horizontalLayout_4.addWidget(self.labelbay2)
        self.file2Button = QtGui.QPushButton(LiFiReceiver)
        self.file2Button.setObjectName(_fromUtf8("file2Button"))
        self.horizontalLayout_4.addWidget(self.file2Button)
        self.bay2button = QtGui.QPushButton(LiFiReceiver)
        self.bay2button.setObjectName(_fromUtf8("bay2button"))
        self.horizontalLayout_4.addWidget(self.bay2button)
        self.verticalLayout_4.addLayout(self.horizontalLayout_4)
        self.plainTextEditbay2 = QtGui.QPlainTextEdit(LiFiReceiver)
        self.plainTextEditbay2.setTextInteractionFlags(QtCore.Qt.TextSelectableByKeyboard|QtCore.Qt.TextSelectableByMouse)
        self.plainTextEditbay2.setObjectName(_fromUtf8("plainTextEditbay2"))
        self.verticalLayout_4.addWidget(self.plainTextEditbay2)
        self.verticalLayout_6 = QtGui.QVBoxLayout()
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        self.labelbay4 = QtGui.QLabel(LiFiReceiver)
        self.labelbay4.setAlignment(QtCore.Qt.AlignCenter)
        self.labelbay4.setObjectName(_fromUtf8("labelbay4"))
        self.horizontalLayout_7.addWidget(self.labelbay4)
        self.file4Button = QtGui.QPushButton(LiFiReceiver)
        self.file4Button.setObjectName(_fromUtf8("file4Button"))
        self.horizontalLayout_7.addWidget(self.file4Button)
        self.bay4button = QtGui.QPushButton(LiFiReceiver)
        self.bay4button.setObjectName(_fromUtf8("bay4button"))
        self.horizontalLayout_7.addWidget(self.bay4button)
        self.verticalLayout_6.addLayout(self.horizontalLayout_7)
        self.plainTextEditbay4 = QtGui.QPlainTextEdit(LiFiReceiver)
        self.plainTextEditbay4.setTextInteractionFlags(QtCore.Qt.TextSelectableByKeyboard|QtCore.Qt.TextSelectableByMouse)
        self.plainTextEditbay4.setObjectName(_fromUtf8("plainTextEditbay4"))
        self.verticalLayout_6.addWidget(self.plainTextEditbay4)
        self.verticalLayout_4.addLayout(self.verticalLayout_6)
        self.gridLayout.addLayout(self.verticalLayout_4, 0, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.statusLabel = QtGui.QLabel(LiFiReceiver)
        self.statusLabel.setObjectName(_fromUtf8("statusLabel"))
        self.horizontalLayout_6.addWidget(self.statusLabel)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem1)
        self.statusStringLabel = QtGui.QLabel(LiFiReceiver)
        self.statusStringLabel.setObjectName(_fromUtf8("statusStringLabel"))
        self.horizontalLayout_6.addWidget(self.statusStringLabel)
        self.verticalLayout.addLayout(self.horizontalLayout_6)

        self.retranslateUi(LiFiReceiver)
        QtCore.QMetaObject.connectSlotsByName(LiFiReceiver)

    def retranslateUi(self, LiFiReceiver):
        LiFiReceiver.setWindowTitle(_translate("LiFiReceiver", "LiFI Based Product Info System", None))
        self.portLabel.setText(_translate("LiFiReceiver", "Port:", None))
        self.baudLabel.setText(_translate("LiFiReceiver", "BaudRate:", None))
        self.baudCB.setItemText(0, _translate("LiFiReceiver", "2400", None))
        self.baudCB.setItemText(1, _translate("LiFiReceiver", "4800", None))
        self.baudCB.setItemText(2, _translate("LiFiReceiver", "9600", None))
        self.baudCB.setItemText(3, _translate("LiFiReceiver", "14400", None))
        self.baudCB.setItemText(4, _translate("LiFiReceiver", "19200", None))
        self.baudCB.setItemText(5, _translate("LiFiReceiver", "28800", None))
        self.baudCB.setItemText(6, _translate("LiFiReceiver", "38400", None))
        self.baudCB.setItemText(7, _translate("LiFiReceiver", "57600", None))
        self.baudCB.setItemText(8, _translate("LiFiReceiver", "76800", None))
        self.baudCB.setItemText(9, _translate("LiFiReceiver", "115200", None))
        self.baudCB.setItemText(10, _translate("LiFiReceiver", "230400", None))
        self.baudCB.setItemText(11, _translate("LiFiReceiver", "250000", None))
        self.baudCB.setItemText(12, _translate("LiFiReceiver", "500000", None))
        self.baudCB.setItemText(13, _translate("LiFiReceiver", "1000000", None))
        self.baudCB.setItemText(14, _translate("LiFiReceiver", "2000000", None))
        self.portButton.setText(_translate("LiFiReceiver", "Open", None))
        self.aboutButton.setText(_translate("LiFiReceiver", "About", None))
        self.labelbay1.setText(_translate("LiFiReceiver", "Bay1", None))
        self.file1Button.setText(_translate("LiFiReceiver", "File", None))
        self.bay1button.setText(_translate("LiFiReceiver", "Edit", None))
        self.plainTextEditbay1.setPlainText(_translate("LiFiReceiver", "this is bay 1", None))
        self.labelbay3.setText(_translate("LiFiReceiver", "Bay3", None))
        self.file3Button.setText(_translate("LiFiReceiver", "File", None))
        self.bay3button.setText(_translate("LiFiReceiver", "Edit", None))
        self.plainTextEditbay3.setPlainText(_translate("LiFiReceiver", "this is bay 3", None))
        self.labelbay2.setText(_translate("LiFiReceiver", "Bay2", None))
        self.file2Button.setText(_translate("LiFiReceiver", "File", None))
        self.bay2button.setText(_translate("LiFiReceiver", "Edit", None))
        self.plainTextEditbay2.setPlainText(_translate("LiFiReceiver", "this is bay 2", None))
        self.labelbay4.setText(_translate("LiFiReceiver", "Bay4", None))
        self.file4Button.setText(_translate("LiFiReceiver", "File", None))
        self.bay4button.setText(_translate("LiFiReceiver", "Edit", None))
        self.plainTextEditbay4.setPlainText(_translate("LiFiReceiver", "this is bay 4", None))
        self.statusLabel.setText(_translate("LiFiReceiver", "Status:", None))
        self.statusStringLabel.setText(_translate("LiFiReceiver", "Port is Closed.", None))

        '''
        Get all seril ports in system and show in the combo box
        '''
        self.portlist = serialenum.enumerate()
        for port in range(len(self.portlist)):
            self.portCB.addItem(self.portlist[port].encode("utf-8"))
        #add loop serial device for testing
        self.portCB.addItem('loop://')
        '''
        About button function
        '''
        self.aboutButton.clicked.connect(self.about)
        '''
        Open button function
        '''
        self.portButton.clicked.connect(self.openPort)
        '''
        Edit/Save button function
        '''
        self.bay4button.clicked.connect(lambda: self.fileSave(4))
        self.bay3button.clicked.connect(lambda: self.fileSave(3))
        self.bay2button.clicked.connect(lambda: self.fileSave(2))
        self.bay1button.clicked.connect(lambda: self.fileSave(1))
        '''
        File select
        '''
        self.file1Button.clicked.connect(lambda: self.selectFile(1))
        self.file2Button.clicked.connect(lambda: self.selectFile(2))
        self.file3Button.clicked.connect(lambda: self.selectFile(3))
        self.file4Button.clicked.connect(lambda: self.selectFile(4))
    def openPort(self):
        #if self.portButton.text()=='Open':
        if (self.portOpen==False):#Open port
            if(self.edit1==True | self.edit2==True | self.edit3==True | self.edit4==True): #If any bay is in editing mode don't open port and show erroe message
                QtGui.QMessageBox.information(self, 'Error', 'Please save the data first!', QtGui.QMessageBox.Ok)
            else:
                data = str(self.portCB.currentText())#Get port Name
                if (data=='loop://'):
                    self.ser = serial.serial_for_url('loop://')
                else:
                    self.ser.port=data#Set Port Name
                    self.ser.baudrate = int(self.baudCB.currentText())  #Get & Set baud Rate
                    self.ser.open()#Open Serial port
                #print self.ser.writeTimeout
                if self.ser.isOpen():#Check if opened successfully
                    self.portButton.setText(_translate("LiFiReceiver", "Close", None))#Modify Button Text to close
                    self.portOpen=True;
                    self.statusStringLabel.setText(_translate("LiFiReceiver", "Port is Opened.", None))
                    self.portCB.setEnabled(False)
                    self.baudCB.setEnabled(False)
                    self.arduinoSim()
                    self.thread1 = threading.Thread(target=self.monitorPort)
                    self.thread1.start()

        else:
            if self.ser.isOpen():#Close port
                self.ser.close()
                self.portButton.setText(_translate("LiFiReceiver", "Open", None))#Modify Button Text to open
                self.portOpen=False
                self.statusStringLabel.setText(_translate("LiFiReceiver", "Port is Closed.", None))
                self.portCB.setEnabled(True)
                self.baudCB.setEnabled(True)
    def about(self):
        QtGui.QMessageBox.information(self, 'About', '           LiFi\n      Project By:\nShubham Shimpi\n   Vaidehi Vedula\n   Ankita Sawant\n Vibhendra Singh', QtGui.QMessageBox.Ok)

    def portWarning(self):
        if(self.portOpen==True):#Warn if port is open before file selection
            QtGui.QMessageBox.information(self, 'Error', 'Please close the port first!', QtGui.QMessageBox.Ok)
            return False
        else :
            return True
    def selectFile(self,bayNo):
        if(self.portWarning()):
            if (bayNo==1):
                self.name1 = str(QtGui.QFileDialog.getOpenFileName(self,'Select File')) #get file name
                if (self.name1!=''):
                    fileObj = open(self.name1,'r')#Open file
                    self.fileData1 = fileObj.read()#Read Fle
                    fileObj.close()#close File
                    if(self.fileData1!=''):#Check if file has valid data
                        self.plainTextEditbay1.setPlainText(_translate("LiFiReceiver", self.fileData1, None))
                        self.statusStringLabel.setText(_translate("LiFiReceiver", 'Imported file: ' + os.path.basename(self.name1), None))
                    else:
                        QtGui.QMessageBox.information(self, 'Error', 'File cannot be empty!', QtGui.QMessageBox.Ok)
            if (bayNo==2):
                self.name2 = str(QtGui.QFileDialog.getOpenFileName(self,'Select File')) #get file name
                if (self.name2!=''):
                    fileObj = open(self.name2,'r')#Open file
                    self.fileData2 = fileObj.read()#Read Fle
                    fileObj.close()#close File
                    if(self.fileData2!=''):#Check if file has valid data
                        self.plainTextEditbay2.setPlainText(_translate("LiFiReceiver", self.fileData2, None))
                        self.statusStringLabel.setText(_translate("LiFiReceiver", 'Imported file: ' + os.path.basename(self.name2), None))
                    else:
                        QtGui.QMessageBox.information(self, 'Error', 'File cannot be empty!', QtGui.QMessageBox.Ok)
            if (bayNo==3):
                self.name3 = str(QtGui.QFileDialog.getOpenFileName(self,'Select File')) #get file name
                if (self.name3!=''):
                    fileObj = open(self.name3,'r')#Open file
                    self.fileData3 = fileObj.read()#Read Fle
                    fileObj.close()#close File
                    if(self.fileData3!=''):#Check if file has valid data
                        self.plainTextEditbay3.setPlainText(_translate("LiFiReceiver", self.fileData3, None))
                        self.statusStringLabel.setText(_translate("LiFiReceiver", 'Imported file: ' + os.path.basename(self.name3), None))
                    else:
                        QtGui.QMessageBox.information(self, 'Error', 'File cannot be empty!', QtGui.QMessageBox.Ok)
            if (bayNo==4):
                self.name4 = str(QtGui.QFileDialog.getOpenFileName(self,'Select File')) #get file name
                if (self.name1!=''):
                    fileObj = open(self.name4,'r')#Open file
                    self.fileData4 = fileObj.read()#Read Fle
                    fileObj.close()#close File
                    if(self.fileData4!=''):#Check if file has valid data
                        self.plainTextEditbay4.setPlainText(_translate("LiFiReceiver", self.fileData4, None))
                        self.statusStringLabel.setText(_translate("LiFiReceiver", 'Imported file: ' + os.path.basename(self.name4), None))
                    else:
                        QtGui.QMessageBox.information(self, 'Error', 'File cannot be empty!', QtGui.QMessageBox.Ok)

    def fileSave(self,bayNo):
        #self.saveFile.show() #Shows dialog box but input isn't retricted
        if(self.portWarning()):
            if (bayNo==1):
                if self.edit1==False : #Edit text
                    self.plainTextEditbay1.setReadOnly(False) #Allow editing
                    self.edit1=True
                    self.bay1button.setText(_translate("LiFiReceiver", "Save", None))
                    self.buffer1 =  self.plainTextEditbay1.toPlainText()   #Create copy of the original text before editing
                    self.file1Button.setEnabled(False)
                    self.statusStringLabel.setText(_translate("LiFiReceiver", 'Editing data for bay 1.', None))
                else :#Save Text
                    if(self.plainTextEditbay1.toPlainText()==''):#Check if data is valid
                        QtGui.QMessageBox.information(self, 'Error', 'Data cannot be empty!', QtGui.QMessageBox.Ok)
                    else:
                        if (self.name1!=''):#if filename is not empty
                            choice=self.dialog(bayNo) #Ask whether to update in the file
                            #print choice
                            if(choice==0):#Resume editing                                pass
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Editing data for bay 1.', None))
                            if(choice==1):
                                self.plainTextEditbay1.setReadOnly(True) #Deny editing
                                self.edit1=False
                                self.bay1button.setText(_translate("LiFiReceiver", "Edit", None))
                                fileObj = open(self.name1,'w')#Open file
                                fileObj.write(self.plainTextEditbay1.toPlainText())
                                fileObj.close()#close File
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Edited data for bay 1 and updated into ' + os.path.basename(self.name1) + '.', None))
                            if(choice==2):
                                self.plainTextEditbay1.setReadOnly(True) #Deny editing
                                self.edit1=False
                                self.bay1button.setText(_translate("LiFiReceiver", "Edit", None))
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Edited data for bay 1.', None))
                            if(choice==3): #Revert changes to text
                                self.plainTextEditbay1.setReadOnly(True) #Deny editing
                                self.edit1=False
                                self.bay1button.setText(_translate("LiFiReceiver", "Edit", None))
                                self.plainTextEditbay1.setPlainText(_translate("LiFiReceiver", self.buffer1, None)) #Reset all changes
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Discarded changes in bay 1 data.', None))
                        else:#if data is not from file
                            if(self.plainTextEditbay1.toPlainText()!=''):#Check if data is valid
                                self.plainTextEditbay1.setReadOnly(True) #Deny editing
                                self.edit1=False
                                self.bay1button.setText(_translate("LiFiReceiver", "Edit", None))
                            else:
                                QtGui.QMessageBox.information(self, 'Error', 'Data cannot be empty!', QtGui.QMessageBox.Ok)
                        self.file1Button.setEnabled(True)
            if (bayNo==2):
                if self.edit2==False : #Edit text
                    self.plainTextEditbay2.setReadOnly(False) #Allow editing
                    self.edit2=True
                    self.bay2button.setText(_translate("LiFiReceiver", "Save", None))
                    self.buffer2 =  self.plainTextEditbay2.toPlainText()   #Create copy of the original text before editing
                    self.file2Button.setEnabled(False)
                    self.statusStringLabel.setText(_translate("LiFiReceiver", 'Editing data for bay 2.', None))
                else :#Save Text
                    if(self.plainTextEditbay2.toPlainText()==''):#Check if data is valid
                        QtGui.QMessageBox.information(self, 'Error', 'Data cannot be empty!', QtGui.QMessageBox.Ok)
                    else:
                        if (self.name2!=''):#if filename is not empty
                            choice=self.dialog(bayNo) #Ask whether to update in the file
                            #print choice
                            if(choice==0):#Resume editing                                pass
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Editing data for bay 2.', None))
                            if(choice==1):
                                self.plainTextEditbay2.setReadOnly(True) #Deny editing
                                self.edit2=False
                                self.bay2button.setText(_translate("LiFiReceiver", "Edit", None))
                                fileObj = open(self.name2,'w')#Open file
                                fileObj.write(self.plainTextEditbay2.toPlainText())
                                fileObj.close()#close File
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Edited data for bay 2 and updated into ' + os.path.basename(self.name2) + '.', None))
                            if(choice==2):
                                self.plainTextEditbay2.setReadOnly(True) #Deny editing
                                self.edit2=False
                                self.bay2button.setText(_translate("LiFiReceiver", "Edit", None))
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Edited data for bay 2.', None))
                            if(choice==3): #Revert changes to text
                                self.plainTextEditbay2.setReadOnly(True) #Deny editing
                                self.edit2=False
                                self.bay2button.setText(_translate("LiFiReceiver", "Edit", None))
                                self.plainTextEditbay2.setPlainText(_translate("LiFiReceiver", self.buffer2, None)) #Reset all changes
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Discarded changes in bay 2 data.', None))
                        else:#if data is not from file
                            if(self.plainTextEditbay2.toPlainText()!=''):#Check if data is valid
                                self.plainTextEditbay2.setReadOnly(True) #Deny editing
                                self.edit2=False
                                self.bay2button.setText(_translate("LiFiReceiver", "Edit", None))
                            else:
                                QtGui.QMessageBox.information(self, 'Error', 'Data cannot be empty!', QtGui.QMessageBox.Ok)
                        self.file2Button.setEnabled(True)
            if (bayNo==3):
                if self.edit3==False : #Edit text
                    self.plainTextEditbay3.setReadOnly(False) #Allow editing
                    self.edit3=True
                    self.bay3button.setText(_translate("LiFiReceiver", "Save", None))
                    self.buffer3 =  self.plainTextEditbay3.toPlainText()   #Create copy of the original text before editing
                    self.file3Button.setEnabled(False)
                    self.statusStringLabel.setText(_translate("LiFiReceiver", 'Editing data for bay 3.', None))
                else :#Save Text
                    if(self.plainTextEditbay3.toPlainText()==''):#Check if data is valid
                        QtGui.QMessageBox.information(self, 'Error', 'Data cannot be empty!', QtGui.QMessageBox.Ok)
                    else:
                        if (self.name3!=''):#if filename is not empty
                            choice=self.dialog(bayNo) #Ask whether to update in the file
                            #print choice
                            if(choice==0):#Resume editing                                pass
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Editing data for bay 3.', None))
                            if(choice==1):
                                self.plainTextEditbay3.setReadOnly(True) #Deny editing
                                self.edit3=False
                                self.bay3button.setText(_translate("LiFiReceiver", "Edit", None))
                                fileObj = open(self.name3,'w')#Open file
                                fileObj.write(self.plainTextEditbay3.toPlainText())
                                fileObj.close()#close File
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Edited data for bay 3 and updated into ' + os.path.basename(self.name3) + '.', None))
                            if(choice==2):
                                self.plainTextEditbay3.setReadOnly(True) #Deny editing
                                self.edit3=False
                                self.bay3button.setText(_translate("LiFiReceiver", "Edit", None))
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Edited data for bay 3.', None))
                            if(choice==3): #Revert changes to text
                                self.plainTextEditbay3.setReadOnly(True) #Deny editing
                                self.edit3=False
                                self.bay3button.setText(_translate("LiFiReceiver", "Edit", None))
                                self.plainTextEditbay3.setPlainText(_translate("LiFiReceiver", self.buffer3, None)) #Reset all changes
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Discarded changes in bay 3 data.', None))
                        else:#if data is not from file
                            if(self.plainTextEditbay3.toPlainText()!=''):#Check if data is valid
                                self.plainTextEditbay3.setReadOnly(True) #Deny editing
                                self.edit3=False
                                self.bay3button.setText(_translate("LiFiReceiver", "Edit", None))
                            else:
                                QtGui.QMessageBox.information(self, 'Error', 'Data cannot be empty!', QtGui.QMessageBox.Ok)
                        self.file3Button.setEnabled(True)
            if (bayNo==4):
                if self.edit4==False : #Edit text
                    self.plainTextEditbay4.setReadOnly(False) #Allow editing
                    self.edit4=True
                    self.bay4button.setText(_translate("LiFiReceiver", "Save", None))
                    self.buffer4 =  self.plainTextEditbay4.toPlainText()   #Create copy of the original text before editing
                    self.file4Button.setEnabled(False)
                    self.statusStringLabel.setText(_translate("LiFiReceiver", 'Editing data for bay 4.', None))
                else :#Save Text
                    if(self.plainTextEditbay4.toPlainText()==''):#Check if data is valid
                        QtGui.QMessageBox.information(self, 'Error', 'Data cannot be empty!', QtGui.QMessageBox.Ok)
                    else:
                        if (self.name4!=''):#if filename is not empty
                            choice=self.dialog(bayNo) #Ask whether to update in the file
                            #print choice
                            if(choice==0):#Resume editing                                pass
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Editing data for bay 4.', None))
                            if(choice==1):
                                self.plainTextEditbay4.setReadOnly(True) #Deny editing
                                self.edit4=False
                                self.bay4button.setText(_translate("LiFiReceiver", "Edit", None))
                                fileObj = open(self.name4,'w')#Open file
                                fileObj.write(self.plainTextEditbay4.toPlainText())
                                fileObj.close()#close File
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Edited data for bay 4 and updated into ' + os.path.basename(self.name4) + '.', None))
                            if(choice==2):
                                self.plainTextEditbay4.setReadOnly(True) #Deny editing
                                self.edit4=False
                                self.bay4button.setText(_translate("LiFiReceiver", "Edit", None))
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Edited data for bay 4.', None))
                            if(choice==3): #Revert changes to text
                                self.plainTextEditbay4.setReadOnly(True) #Deny editing
                                self.edit4=False
                                self.bay4button.setText(_translate("LiFiReceiver", "Edit", None))
                                self.plainTextEditbay4.setPlainText(_translate("LiFiReceiver", self.buffer4, None)) #Reset all changes
                                self.statusStringLabel.setText(_translate("LiFiReceiver", 'Discarded changes in bay 4 data.', None))
                        else:#if data is not from file
                            if(self.plainTextEditbay4.toPlainText()!=''):#Check if data is valid
                                self.plainTextEditbay4.setReadOnly(True) #Deny editing
                                self.edit4=False
                                self.bay4button.setText(_translate("LiFiReceiver", "Edit", None))
                            else:
                                QtGui.QMessageBox.information(self, 'Error', 'Data cannot be empty!', QtGui.QMessageBox.Ok)
                        self.file4Button.setEnabled(True)

    def dialog(self,bayNo):
        self.labeltext = "You are about to change the data for bay " + str(bayNo) + ".\nWould you like to update the same in the original file?\nCancel:> Discard changes\nClose:> Back to edit mode!"
        self.saveFile.label.setText(_translate("updateFileDialog", self.labeltext, None))
        '''
        Show dialog box and prevent main window inputs
        '''
        self.output = self.saveFile.exec_()
        return self.output
    def arduinoSim(self):
        print 'In arduinoSim'
        self.ard = QtGui.QWidget()
        self.ard.resize(250, 150)
        self.ard.move(300, 300)
        self.ard.setWindowTitle('Arduino Simluator')
        #print 'Show'
        #ard.show()
        G1 = QtGui.QPushButton('G1', self)
        G2 = QtGui.QPushButton('G2', self)
        G3 = QtGui.QPushButton('G3', self)
        G4 = QtGui.QPushButton('G4', self)
        G1.clicked.connect(lambda : self.sendRequest('G1'))
        G2.clicked.connect(lambda : self.sendRequest('G2'))
        G3.clicked.connect(lambda : self.sendRequest('G3'))
        G4.clicked.connect(lambda : self.sendRequest('G4'))
        quitBtn = QtGui.QPushButton('Quit', self)
        quitBtn.clicked.connect(self.quitArduinoSim)
        layout = QtGui.QVBoxLayout(self.ard)
        layout.addWidget(G1)
        layout.addWidget(G2)
        layout.addWidget(G3)
        layout.addWidget(G4)
        layout.addWidget(quitBtn)

        #self.ard.open()
        self.ard.show()#Press esc to close the dialog
        #print 'raise_'
        #ard.raise_()
    def showMe(self,id):
        print id + 'pressed'
    def quitArduinoSim(self):
        self.ard.close()
    def sendRequest(self,id):
        try:
            if (id=='G1'):
                self.ser.write('<G1></G>\x04')
            if (id=='G2'):
                self.ser.write('<G2></G>\x04')
            if (id=='G3'):
                self.ser.write('<G3></G>\x04')
            if (id=='G4'):
                self.ser.write('<G4></G>\x04')
        except ValueError:
            pass
        #self.readData()
    #def readData(self):
        #data= self.ser.readline()
        #if (data=='<G1></G>\r\n'):
            #self.ser.write(self.plainTextEditbay1.toPlainText() + '')
            #self.ser.flush()
            #print 'To Arduino' + self.readline('\x04')
        #if (data=='<G2></G>\r\n'):
            #self.ser.write(self.plainTextEditbay2.toPlainText() + '\x04')
            #self.ser.flush()
            #print 'To Arduino' + self.readline('\x04')
        #if (data=='<G3></G>\r\n'):
            #self.ser.write(self.plainTextEditbay3.toPlainText() + '\x04')
            #self.ser.flush()
            #print 'To Arduino' + self.readline('\x04')
        #if (data=='<G4></G>\r\n'):
            #self.ser.write(self.plainTextEditbay4.toPlainText() + '\x04')
            #self.ser.flush()
            #print 'To Arduino' + self.readline('\x04')

    def monitorPort(self):
        while self.portOpen:#Monitor and respond to arduino while port is open
            try:
                #data= self.ser.readline()#Blocking call to read line from arduino
                data = self.readEOT('\x04')
                #add custom readline command for serial with wait time if in between consecutive byte reads if graphics isn't responding
                if (data=='<G1></G>'):
                    print 'Received G1'
                    temp = '<P1>' + self.plainTextEditbay1.toPlainText() + '</P>' + '\x04'
                    self.ser.write(bytes(temp))#Send bay 1 data with delimiter '\03'
                    self.ser.flush()    #Wait for data to be written
                    #print 'To Arduino: ' + self.readEOT('\x04') #Simulate arduino reception for verification
                elif (data=='<G2></G>'):
                    print 'Received G2'
                    temp = '<P2>' + self.plainTextEditbay2.toPlainText() + '</P>' + '\x04'
                    self.ser.write(bytes(temp))
                    self.ser.flush()
                    #print 'To Arduino: ' +  self.readEOT('\x04')
                elif (data=='<G3></G>'):
                    print 'Received G3'
                    temp = '<P3>' + self.plainTextEditbay3.toPlainText() + '</P>' + '\x04'
                    self.ser.write(bytes(temp))
                    self.ser.flush()
                    #print 'To Arduino: ' +  self.readEOT('\x04')
                elif (data=='<G4></G>'):
                    print 'Received G4'
                    temp = '<P4>' + self.plainTextEditbay4.toPlainText() + '</P>' + '\x04'
                    self.ser.write(bytes(temp))
                    self.ser.flush()
                    #print 'To Arduino: ' +  self.readEOT('\x04')
                else:#For debugging
                    if (len(data)>1):
                        print 'Received: ' + data
            except serial.SerialException:#Requires when closing serial port the readline() function freezes
                break
            except TypeError:
                break

    def readEOT(self,eol):
        bfr=''
        while True:
            try:
                bfr += self.ser.read(self.ser.inWaiting())
                index = bfr.find(eol)
                if (index):
                    return bfr[0:index]
            except ValueError:#If serial port is closed this function freezes
                return 0
            except IOError:
                return 0
            #sleep(0.1)



if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    ex = Ui_LiFiReceiver()
    ex.show()
    sys.exit(app.exec_())
