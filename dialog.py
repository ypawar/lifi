# -*- coding: utf-8 -*-
# Form implementation generated from reading ui file 'fileSaveDialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!
from PyQt4 import QtCore, QtGui
import sys
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s
try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)
class Ui_updateFileDialog(QtGui.QDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi(self)
        self.setModal (True)
    def setupUi(self, updateFileDialog):
        updateFileDialog.setObjectName(_fromUtf8("updateFileDialog"))
        updateFileDialog.resize(275, 89)
        updateFileDialog.setModal(False)
        self.verticalLayout_2 = QtGui.QVBoxLayout(updateFileDialog)
        self.verticalLayout_2.setSizeConstraint(QtGui.QLayout.SetFixedSize)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(updateFileDialog)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.buttonBox = QtGui.QDialogButtonBox(updateFileDialog)
        self.buttonBox.setLayoutDirection(QtCore.Qt.LeftToRight)
	#self.buttonBox.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.buttonBox.setAutoFillBackground(False)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.No|QtGui.QDialogButtonBox.Yes)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.retranslateUi(updateFileDialog)
        #QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), updateFileDialog.accept)
        #QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), updateFileDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(updateFileDialog)
        #print self.buttonBox.clicked(QtGui.QDialogButtonBox.Cancel).connect(self.cancel)
        self.buttonBox.button(QtGui.QDialogButtonBox.Cancel).clicked.connect(self.cancel)
        self.buttonBox.button(QtGui.QDialogButtonBox.No).clicked.connect(self.no)
        self.buttonBox.button(QtGui.QDialogButtonBox.Yes).clicked.connect(self.yes)
        #self.buttonBox.button(QtGui.QDialogButtonBox.Reset).clicked.connect(foo)
    def retranslateUi(self, updateFileDialog):
        updateFileDialog.setWindowTitle(_translate("updateFileDialog", "Update In File ?", None))
        #self.label.setText(_translate("updateFileDialog", "You are about to change the data for this bay. \n"
#"Would you like to update the same in the original file?", None))
    def yes(self):
        self.done(1)
    def no(self):
        self.done(2)
    def cancel(self):
        self.done(3)          